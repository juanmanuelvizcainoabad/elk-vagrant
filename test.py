from datetime import datetime
from elasticsearch import Elasticsearch
import pytz
from pytz import timezone
es = Elasticsearch(['http://172.31.102.10:9200'])
doc = {
    'server': 'vxmaa-01',
    'tipus_server': "mail",
    'df' : '80',
    'timestamp': datetime.utcnow(),
}

res = es.index(index="espaivxma", body=doc, doc_type='my_type')
print(res['created'])


#curl  'http://172.31.102.10:9200/_cat/indices?v'
#curl -XDELETE  'http://172.31.102.10:9200/espaivxma'
#curl -GET http://172.31.102.10:9200/espaivxma/_mapping |python -m json.tool
