#!/bin/bash

ip="172.31.102.10"


echo "oracle-java8-installer oracle-java8-installer/local string" | /usr/bin/debconf-set-selections
echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 boolean true"| /usr/bin/debconf-set-selections
echo "oracle-java8-installer shared/present-oracle-license-v1-1 note" | /usr/bin/debconf-set-selections



add-apt-repository ppa:webupd8team/java -y
apt-get update
apt-get install oracle-java8-installer oracle-java8-set-default apt-transport-https -y

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch |  apt-key add -

echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" |  tee -a /etc/apt/sources.list.d/elastic-5.x.list


apt-get update &&  apt-get install elasticsearch kibana -y
update-rc.d elasticsearch defaults 95 10

echo "network.host: $ip" >> /etc/elasticsearch/elasticsearch.yml
echo "server.host: \"$ip\"" >> /etc/kibana/kibana.yml
echo "elasticsearch.url: \"http://$ip:9200\""  >> /etc/kibana/kibana.yml
/etc/init.d/elasticsearch start
/etc/init.d/kibana start
